FROM openjdk:8-jdk-alpine
EXPOSE 8092
ADD target/*.jar cms-degree-application.jar
ENTRYPOINT ["java", "-jar", "/cms-degree-application.jar"]