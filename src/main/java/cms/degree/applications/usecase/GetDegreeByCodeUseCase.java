package cms.degree.applications.usecase;

import cms.degree.applications.service.client.Degree;
import cms.degree.applications.service.client.DegreeServiceClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class GetDegreeByCodeUseCase {

    @Autowired
    private final DegreeServiceClient degreeServiceClient;

    public GetDegreeByCodeUseCase(DegreeServiceClient degreeServiceClient) {
        this.degreeServiceClient = degreeServiceClient;
    }

    @Transactional
    public Degree retrieveDegreeByCode(String degreeCode) throws Exception {
        return degreeServiceClient.retrieveDegreeByCode(degreeCode);
    }
}
