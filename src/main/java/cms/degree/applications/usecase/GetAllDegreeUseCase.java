package cms.degree.applications.usecase;

import cms.degree.applications.service.client.Degree;
import cms.degree.applications.service.client.DegreeServiceClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class GetAllDegreeUseCase {

    @Autowired
    private final DegreeServiceClient degreeServiceClient;


    public GetAllDegreeUseCase(DegreeServiceClient degreeServiceClient) {
        this.degreeServiceClient = degreeServiceClient;
    }

    @Transactional
    public ResponseEntity<Degree[]> retrieveAllDegree() throws Exception {
        return degreeServiceClient.retrieveAllDegree();
    }
}
