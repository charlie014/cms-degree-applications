package cms.degree.applications.exception;

public class DegreeServiceClientServerErrorException extends RuntimeException {
    public DegreeServiceClientServerErrorException(String message) {
        super(message);
    }
}
