package cms.degree.applications.exception;

public class NoContentDegreeListException extends Exception {

    public NoContentDegreeListException(String msg) {
        super(msg);
    }
}
