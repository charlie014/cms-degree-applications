package cms.degree.applications.exception;

public class DegreeExistException extends RuntimeException {
    public DegreeExistException(String message) {
        super(message);
    }
}
