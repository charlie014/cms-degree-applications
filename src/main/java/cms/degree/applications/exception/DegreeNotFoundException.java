package cms.degree.applications.exception;

import javassist.NotFoundException;

public class DegreeNotFoundException extends NotFoundException {

    public DegreeNotFoundException(String message) {
        super(message);
    }
}

