package cms.degree.applications.service.client;

import cms.degree.applications.configuration.DegreeConfiguration;
import cms.degree.applications.exception.DegreeExistException;
import cms.degree.applications.exception.DegreeNotFoundException;
import cms.degree.applications.exception.DegreeServiceClientServerErrorException;
import cms.degree.applications.exception.NoContentDegreeListException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;


@Service
public class DegreeServiceClient {

    private final DegreeConfiguration restTemplate = new DegreeConfiguration();

    private String baseURL;

    public DegreeServiceClient(@Value("${degree.service.host}") String host,
                               @Value("${degree.service.port}") int port) {
        this.baseURL = "http://" + host + ":" + port + "/api/service";
    }

    public ResponseEntity<Degree[]> retrieveAllDegree() throws Exception {
        String retrieveAllDegreeUrl = baseURL + "/alldegree";
        ResponseEntity<Degree[]> response = restTemplate.getForEntity(
                retrieveAllDegreeUrl,
                Degree[].class);
        if (response.getStatusCode() == HttpStatus.OK) {
            return response;
        } else if (response.getStatusCode().value() == 204) {
            throw new NoContentDegreeListException("No Content Degree List");
        }
        throw new Exception("General Exception");
    }

    public Degree retrieveDegreeByCode(String degreeCode) throws Exception {
        String retrieveDegreeByCodeUrl = baseURL + "/degree/{degreeCode}";
        try  {
            ResponseEntity<Degree> response = restTemplate.getForEntity(
                retrieveDegreeByCodeUrl,
                Degree.class,
                degreeCode);
            return response.getBody();
        } catch (HttpClientErrorException e) {
            if (e.getStatusCode() == HttpStatus.INTERNAL_SERVER_ERROR) {
                throw new DegreeServiceClientServerErrorException(e.getMessage());
            } else if (e.getStatusCode() == HttpStatus.NOT_FOUND) {
                throw new DegreeNotFoundException("Unable to find degree code " + degreeCode);
            }
            throw new Exception("General Exception");
        }
    }

    public Degree insertNewDegree(Degree requestDegree) {
        String insertNewDegreeUrl = baseURL + "/degree/create";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Degree> request = new HttpEntity<>(requestDegree, headers);

        try {
            ResponseEntity<Degree> response = restTemplate.postForEntity(
                    insertNewDegreeUrl,
                    request,
                    Degree.class
            );
            return response.getBody();
        } catch (HttpClientErrorException e) {
            if ((e.getStatusCode() == HttpStatus.INTERNAL_SERVER_ERROR)) {
                throw new DegreeServiceClientServerErrorException(e.getMessage());
            } else if (e.getStatusCode() == HttpStatus.BAD_REQUEST) {
                throw new DegreeExistException("Degree already exist");
            }
                throw e;
        }
    }
}
