package cms.degree.applications.controller;

import cms.degree.applications.service.client.Degree;
import cms.degree.applications.usecase.GetAllDegreeUseCase;
import cms.degree.applications.usecase.GetDegreeByCodeUseCase;
import cms.degree.applications.usecase.InsertNewDegreeUseCase;
import com.jfilter.filter.FieldFilterSetting;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/api/apps")
public class DegreeAppsController {

    @Autowired
    private final GetAllDegreeUseCase getAllDegreeUseCase;

    @Autowired
    private final GetDegreeByCodeUseCase getDegreebyCodeUseCase;

    @Autowired
    private final InsertNewDegreeUseCase insertNewDegreeUseCase;

    @Autowired
    public DegreeAppsController(GetAllDegreeUseCase getAllDegreeUseCase,
                                GetDegreeByCodeUseCase getDegreebyCodeUseCase,
                                InsertNewDegreeUseCase insertNewDegreeUseCase
    ) {
        this.getAllDegreeUseCase = getAllDegreeUseCase;
        this.getDegreebyCodeUseCase = getDegreebyCodeUseCase;
        this.insertNewDegreeUseCase = insertNewDegreeUseCase;
    }

    @ApiOperation(
            value = "Retrieve all degree",
            notes = "Retrieve list of degrees",
            httpMethod = "GET",
            produces = APPLICATION_JSON_VALUE
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "Retrieve list of degrees successfully"),
            @ApiResponse(code = 404, message = "Degree not found"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.GET, value = "/degree/getAllDegree")
    @FieldFilterSetting(className = Degree.class, fields = {"id", "type", "description", "guaranteedScore", "lowestAtar"})
    @ResponseBody
    public ResponseEntity<Degree[]> retrieveAllDegrees() throws Exception {
        return getAllDegreeUseCase.retrieveAllDegree();
    }

    @ApiOperation(
            value = "Retrieve degree details",
            notes = "Retrieve degree",
            httpMethod = "GET",
            produces = APPLICATION_JSON_VALUE
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "Retrieve degree details successfully"),
            @ApiResponse(code = 404, message = "Degree not found"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.GET, value = "/degree/details/{degreeCode}", produces = APPLICATION_JSON_VALUE)
    @FieldFilterSetting(className = Degree.class, fields = {"id"})
    @ResponseBody
    public Degree retrieveDegreeByCode(
            @ApiParam(name = "degreeCode", required = true, value = "The degree code")
            @PathVariable("degreeCode") String degreeCode) throws Exception {

        return getDegreebyCodeUseCase.retrieveDegreeByCode(degreeCode);
    }

    @ApiOperation(value = "Create new degree", notes = "Create new degree", produces = APPLICATION_JSON_VALUE)
    @ApiResponses({
            @ApiResponse(code = 200, message = "Create new degree successfully"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @RequestMapping(method = RequestMethod.POST, value = "/degree/create", produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public Degree insertNewDegree(
            @ApiParam(required = true)
            @RequestBody Degree newDegree) throws Exception {
        return insertNewDegreeUseCase.insertNewDegree(newDegree);
    }
}
