import {CurrentListOfDegreeResponse} from "../models/ListOfDegreeResponse";
import {DegreeDetailsResponse} from "../models/DegreeDetailsResponse";
import responseToJson from "../utils/responseToJson";

export interface Integration {
    getListOfDegrees(): Promise<CurrentListOfDegreeResponse>;

    getDegreeDetails(
        degreeCode: string
    ): Promise<DegreeDetailsResponse>;
}

const Integration: Integration = {
    async getListOfDegrees() {
        return fetch(
            `/apps/api/degree/getAllDegree`,
            {
                method: "GET",
                credentials: "include"
            }
        ).then(responseToJson);
    },

    async getDegreeDetails(degreeCode: string) {
        return fetch(
            `/apps/api/degree/details/${degreeCode}`,
            {
                method: "GET",
                credentials: "include"
            }
        ).then(responseToJson);
    }
};

export default Integration;