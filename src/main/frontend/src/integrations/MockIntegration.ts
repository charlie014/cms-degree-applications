import { Integration } from "./Integration";

const MockIntegration: Integration = {
    getListOfDegrees: async () =>  {
        return new Promise((resolve => {
            setTimeout(() => {
                return resolve({
                    degree: [
                        {
                            degreeCode: "C2000",
                            degreeTitle: "Bachelor of Information Technology",
                            lowestScore: 80,
                            campus: "Clayton"
                        },
                        {
                            degreeCode: "C2001",
                            degreeTitle:"Bachelor of Computer Science",
                            lowestScore: 85,
                            campus: "Clayton"
                        },
                        {
                            degreeCode: "B2000",
                            degreeTitle: "Bachelor of Commerce",
                            lowestScore: 70,
                            campus: "Caufield"
                        }
                    ],
                    totalDegree: 5,
                    totalPages: 2
                });
            }, 1000)
        }))
    },

    getDegreeDetails: async () => {
        return new Promise((resolve => {
            setTimeout(() => {
                return resolve({
                    id: 1,
                    degreeCode: "C2000",
                    degreeTitle: "Bachelor of Information Technology",
                    description: "Bachelor for IT students",
                    campus: "Clayton",
                    type: "Bachelor",
                    lowestScore: 80,
                    guaranteedScore: 75,
                    lowestAtar: 79
                })
            })
        }))
    }
};

export default MockIntegration;