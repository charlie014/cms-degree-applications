import MockIntegration from "../integrations/MockIntegration";
import {Integration} from "../integrations/Integration";
import getDegreeDetailsEffects from "./getDegreeDetailsEffects";
import AppInitialState from "../state/AppState";
import {AppAction} from "../state/AppAction";
import {DegreeDetailsResponse} from "../models/DegreeDetailsResponse";
import getListOfDegreeEffects from "./getListOfDegreeEffects";

describe("getDegreeDetailsEffects", () => {
    it("should return failure if the request is unsuccessfully", async () => {
        const integration = {
            ...MockIntegration,
            getDegreeDetails: async () => Promise.reject("FAIL")
        } as Integration;

        const result = await getDegreeDetailsEffects(
            AppInitialState,
            {} as AppAction,
            { integration }
        );

        expect(result).toEqual({
            type: "DEGREE_DETAILS_FAILURE",
            payload: new Error("FAIL")
        })
    });

    it("should return degree details if the request is successfully", async () => {
        const degreeDetailsResponse = {
            id: 1,
            degreeCode: "C2000",
            degreeTitle: "Bachelor of Information Technology",
            description: "Bachelor for IT students",
            type: "Bachelor",
            campus: "Clayton",
            lowestScore: 80,
            guaranteedScore: 75,
            lowestAtar: 79
        } as DegreeDetailsResponse;

        const integration = {
            ...MockIntegration,
            getDegreeDetails: async () => degreeDetailsResponse
        } as Integration;

        const result = await getDegreeDetailsEffects(
            AppInitialState,
            {} as AppAction,
            { integration }
        );

        expect(result).toEqual({
            type: "DEGREE_DETAILS_SUCCESS",
            payload: {
                id: 1,
                degreeCode: "C2000",
                degreeTitle: "Bachelor of Information Technology",
                description: "Bachelor for IT students",
                type: "Bachelor",
                campus: "Clayton",
                lowestScore: 80,
                guaranteedScore: 75,
                lowestAtar: 79
            }
        })
    });
})