import {Integration} from "../integrations/Integration";
import {SideEffect} from "../hooks/useReducerWithEffects";
import { AppState } from "../state/AppState";
import {AppAction} from "../state/AppAction";

export interface AppEffectContext {
    integration: Integration
}

export type AppEffects = SideEffect<AppState, AppAction, AppEffectContext>