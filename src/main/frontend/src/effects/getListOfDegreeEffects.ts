import {AppEffects} from "./AppEffects";
import {currentListOfDegreeFailure, currentListOfDegreeSuccess} from "../state/AppAction";

const getListOfDegreeEffects: AppEffects = async (state, action, context) => {
    try {
        const currentListOfDegreeResponse = await context.integration.getListOfDegrees();
        return currentListOfDegreeSuccess(currentListOfDegreeResponse);
    } catch (error) {
        return currentListOfDegreeFailure(new Error(error));
    }
};

export default getListOfDegreeEffects;