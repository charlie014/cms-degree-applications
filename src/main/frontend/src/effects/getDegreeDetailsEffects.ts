import {AppEffects} from "./AppEffects";
import {degreeDetailsFailure, degreeDetailsSuccess} from "../state/AppAction";

const getDegreeDetailsEffects: AppEffects = async (state, action, context) => {
    try {
        const degreeDetailsResponse = await context.integration.getDegreeDetails(
            state.degreeDetails.degreeCode
        );

        return degreeDetailsSuccess(degreeDetailsResponse);
    } catch(error) {
        return degreeDetailsFailure(new Error(error));
    }
};

export default getDegreeDetailsEffects;