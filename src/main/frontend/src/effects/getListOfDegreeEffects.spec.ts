import MockIntegration from "../integrations/MockIntegration";
import {Integration} from "../integrations/Integration";
import getListOfDegreeEffects from "./getListOfDegreeEffects";
import AppInitialState, {DegreeSummaryContent} from "../state/AppState";
import {AppAction} from "../state/AppAction";
import {ListOfDegreeResponse} from "../models/ListOfDegreeResponse";

describe("getListOfDegreeEffects", () => {
    it("should return failure if the request is unsuccessfully ", async () => {
        const integration = {
            ...MockIntegration,
            getListOfDegrees: async () => Promise.reject("FAIL"),
        } as Integration;

        const result = await getListOfDegreeEffects(
            AppInitialState,
            {} as AppAction,
            {integration}
        );

        expect(result).toEqual({
                type: "CURRENT_DEGREE_FAILURE",
                payload: new Error("FAIL")
            }
        )
    });

    it("should return list of degrees if the request is successfully", async () => {
        const degreeListResponse = {
            degree: [
                {
                    degreeCode: "C2000",
                    degreeTitle:"Bachelor of Information Technology",
                    lowestScore: 80,
                    campus: "Clayton"
                },
                {
                    degreeCode: "C2001",
                    degreeTitle:"Bachelor of Computer Science",
                    lowestScore: 85,
                    campus: "Clayton"
                },
                {
                    degreeCode: "B2000",
                    degreeTitle:"Bachelor of Commerce",
                    lowestScore: 70,
                    campus: "Caufield"
                }
            ],
            totalPages: 1,
            totalDegree: 3
        } as ListOfDegreeResponse;
        const integration = {
            ...MockIntegration,
            getListOfDegrees: async () => degreeListResponse
        } as Integration;

        const result = await getListOfDegreeEffects(
            AppInitialState,
            {} as AppAction,
            { integration }
        );

        expect(result).toEqual(
            {
                type: "CURRENT_DEGREE_SUCCESS",
                payload: {
                    degree: [
                        {
                            degreeCode: "C2000",
                            degreeTitle:"Bachelor of Information Technology",
                            lowestScore: 80,
                            campus: "Clayton"
                        },
                        {
                            degreeCode: "C2001",
                            degreeTitle:"Bachelor of Computer Science",
                            lowestScore: 85,
                            campus: "Clayton"
                        },
                        {
                            degreeCode: "B2000",
                            degreeTitle:"Bachelor of Commerce",
                            lowestScore: 70,
                            campus: "Caufield"
                        }
                    ],
                    totalPages: 1,
                    totalDegree: 3
                }
            }
        )
    })
});