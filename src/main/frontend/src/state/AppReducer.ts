/*
    This is the place will trigger the action and reload the state
 */

import {Reducer} from "react";
import {AppState} from "./AppState";
import {AppAction} from "./AppAction";
import {AsyncLoadStatus} from "../models/AsyncLoadStatus";

const AppReducer: Reducer<AppState, AppAction> = (
    prevState,
    action
): AppState => {
    switch (action.type) {
        case "LOAD_CURRENT_LIST_OF_DEGREE": {
            const {} = action.payload;

            return {
                ...prevState,
                currentDegree: {
                    ...prevState.currentDegree,
                    loadStatus: AsyncLoadStatus.LOADING
                },
            };
        }

        case "CURRENT_LIST_OF_DEGREE_FAILURE": {
            return {
                ...prevState,
                currentDegree: {
                    ...prevState.currentDegree,
                    loadStatus: AsyncLoadStatus.FAILURE,
                },
            };
        }

        case "CURRENT_LIST_OF_DEGREE_SUCCESS": {
            return {
                ...prevState,
                currentDegree: {
                    ...prevState.currentDegree,
                    totalPages: action.payload.totalPages,
                    totalDegrees: action.payload.totalDegree,
                    loadStatus: AsyncLoadStatus.SUCCESS,
                    degree: action.payload.degree,
                },
            };
        }

        case "LOAD_DEGREE_DETAILS": {
            const {
                degreeCode = prevState.degreeDetails.degreeCode
            } = action.payload;

            return {
                ...prevState,
                degreeDetailsLoadStatus: AsyncLoadStatus.LOADING,
                degreeDetails: {
                    ...prevState.degreeDetails,
                    degreeCode: degreeCode
                },
            };
        }

        case "DEGREE_DETAILS_SUCCESS": {
            return {
                ...prevState,
                degreeDetailsLoadStatus: AsyncLoadStatus.SUCCESS,
                degreeDetails: {
                    id: action.payload.id,
                    degreeCode: action.payload.degreeCode,
                    degreeTitle: action.payload.degreeTitle,
                    description: action.payload.description,
                    type: action.payload.type,
                    campus: action.payload.campus,
                    lowestScore: action.payload.lowestScore,
                    guaranteedScore: action.payload.guaranteedScore,
                    lowestAtar: action.payload.lowestAtar
                },
            };
        }

        case "DEGREE_DETAILS_FAILURE": {
            return {
                ...prevState,
                degreeDetailsLoadStatus: AsyncLoadStatus.FAILURE,
                degreeDetails: {
                    ...prevState.degreeDetails,
                },
            };
        }
        default: {
            return prevState;
        }
    }
};

export default AppReducer;
