import {ActionWithEffects} from "../hooks/useReducerWithEffects";
import {AppState} from "./AppState";
import {AppEffectContext} from "../effects/AppEffects";
import {CurrentListOfDegreeResponse, ListOfDegreeResponse} from "../models/ListOfDegreeResponse";
import {DegreeDetailsResponse} from "../models/DegreeDetailsResponse";
import getListOfDegreeEffects from "../effects/getListOfDegreeEffects";
import getDegreeDetailsEffects from "../effects/getDegreeDetailsEffects";

export interface Action<T, P>
    extends ActionWithEffects<AppState, AppAction, AppEffectContext> {
    type: T;
    payload: P
}

interface InitialDegreeSummaryParameters {
    pageNumber?: number;
    pageSize?: number;
}

interface InitialDegreeDetailsParameters {
    degreeCode: string;
}

export type CurrentListOfDegreeLoadingAction = Action<
    "LOAD_CURRENT_LIST_OF_DEGREE",
    {}
>;

export const loadCurrentListOfDegree = (): CurrentListOfDegreeLoadingAction => ({
    type: "LOAD_CURRENT_LIST_OF_DEGREE",
    payload: {},
    effects: getListOfDegreeEffects,
});

// Fail loading action
export type CurrentListOfDegreeFailureAction = Action<"CURRENT_LIST_OF_DEGREE_FAILURE", Error>;
export const currentListOfDegreeFailure = (
    error: Error
): CurrentListOfDegreeFailureAction => ({
    type: "CURRENT_LIST_OF_DEGREE_FAILURE",
    payload: error,
});

// Success loading action
export type CurrentListOfDegreeSuccessAction = Action<
    "CURRENT_LIST_OF_DEGREE_SUCCESS",
    CurrentListOfDegreeResponse
    >;
export const currentListOfDegreeSuccess = (
    details: ListOfDegreeResponse
): CurrentListOfDegreeSuccessAction => <Action<"CURRENT_LIST_OF_DEGREE_SUCCESS", CurrentListOfDegreeResponse>>({
    type: "CURRENT_LIST_OF_DEGREE_SUCCESS",
    payload: details,
});

// Navigation to different page
export type CurrentListOfDegreeNavigatePageAction = Action<
    "NAVIGATE_DIFFERENT_PAGE",
    number
    >;
export const navigatePageAction = (
    pageNumber: number
): CurrentListOfDegreeNavigatePageAction => ({
    type: "NAVIGATE_DIFFERENT_PAGE",
    payload: pageNumber,
});

// loading action for item details
export type DegreeDetailsLoadingAction = Action<
    "LOAD_DEGREE_DETAILS",
    InitialDegreeDetailsParameters
    >;

export const loadDegreeDetails = (
    initialParameters: InitialDegreeDetailsParameters
): DegreeDetailsLoadingAction => ({
    type: "LOAD_DEGREE_DETAILS",
    payload: initialParameters,
    effects: getDegreeDetailsEffects,
});

// load the item details successfully
export type DegreeDetailsSuccessAction = Action<
    "DEGREE_DETAILS_SUCCESS",
    DegreeDetailsResponse
    >;

export const degreeDetailsSuccess = (
    details: DegreeDetailsResponse
): DegreeDetailsSuccessAction => ({
    type: "DEGREE_DETAILS_SUCCESS",
    payload: details,
});

// load the item failure
export type DegreeDetailsFailureAction = Action<"DEGREE_DETAILS_FAILURE", Error>;

export const degreeDetailsFailure = (error: Error): DegreeDetailsFailureAction => ({
    type: "DEGREE_DETAILS_FAILURE",
    payload: error,
});

export type AppAction =
    | CurrentListOfDegreeLoadingAction
    | CurrentListOfDegreeFailureAction
    | CurrentListOfDegreeSuccessAction
    | CurrentListOfDegreeNavigatePageAction
    | DegreeDetailsLoadingAction
    | DegreeDetailsSuccessAction
    | DegreeDetailsFailureAction