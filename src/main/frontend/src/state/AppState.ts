import {AsyncLoadStatus} from "../models/AsyncLoadStatus";


export interface DegreeSummaryContent {
    totalPages: number;
    totalDegrees: number;
    pageNumber: number;
    pageSize: number;
    loadStatus: AsyncLoadStatus;
    degree: Degrees[];
}

export interface Degrees {
    degreeCode: string;
    degreeTitle: string;
    lowestScore: number;
    campus: string;
}

export interface DegreeDetails extends Degrees {
    id: number;
    type: string;
    description: string;
    guaranteedScore: number;
    lowestAtar: number;
}

export interface AppState {
    currentDegree: DegreeSummaryContent;
    degreeDetailsLoadStatus: AsyncLoadStatus;
    degreeDetails: DegreeDetails;
}

const AppInitialState: AppState = {
    currentDegree: {
        totalPages: 1,
        totalDegrees: 5,
        pageNumber: 1,
        pageSize: 10,
        loadStatus: AsyncLoadStatus.INITIAL,
        degree: [
            {
                degreeCode: "C2000",
                degreeTitle: "Bachelor of Information Technology",
                campus: "Clayton",
                lowestScore: 80
            },
            {
                degreeCode: "C2001",
                degreeTitle: "Bachelor of Computer Science",
                campus: "Clayton",
                lowestScore: 85
            },
            {
                degreeCode: "C3000",
                degreeTitle: "Bachelor of Commerce",
                campus: "Caufield",
                lowestScore: 70
            }
        ]
    },
    degreeDetailsLoadStatus: AsyncLoadStatus.INITIAL,
    degreeDetails: {
        id: 1,
        degreeCode: "C2000",
        degreeTitle: "Bachelor of Information Technology",
        type: "Bachelor",
        campus: "Clayton",
        description: "Bachelor for IT students",
        lowestScore: 80,
        guaranteedScore: 75,
        lowestAtar: 79
    }
};

export default AppInitialState;

