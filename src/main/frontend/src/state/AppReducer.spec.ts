import AppInitialState, { AppState, DegreeDetails } from "./AppState";
import * as AppAction from "./AppAction";
import AppReducer from "./AppReducer";
import { AsyncLoadStatus } from "../models/AsyncLoadStatus";
import {
    ListOfDegreeResponse,
    DegreeDetailsResponseLine, CurrentListOfDegreeResponse,
} from "../models/ListOfDegreeResponse";
import { DegreeDetailsResponse } from "../models/DegreeDetailsResponse";

describe("AppReducer", () => {
    describe("Any unrecognised action", () => {
        it("should return exact the same state as-is", () => {
            //given
            const state = mockState();

            //when
            const deliberatelyBrokenAction = {} as AppAction.CurrentListOfDegreeSuccessAction;
            const result = AppReducer(state, deliberatelyBrokenAction);

            //that
            expect(result).toBe(state);
        });
    });

    describe("LOAD_CURRENT_LIST_OF_DEGREE", () => {
        it("should not modify the current total Degrees state", () => {
            //given
            const state = mockState();

            //when
            const action = AppAction.loadCurrentListOfDegree();
            const result = AppReducer(state, action);

            //that
            expect(result.currentDegree.totalDegrees).toEqual(5);
        });

        it("should take pageSize as a parameter", () => {
            const state = mockState();

            const action = AppAction.loadCurrentListOfDegree();
            const result = AppReducer(state, action);

            expect(result.currentDegree.pageSize).toEqual(10);
        });

        it("should take pageNumber as a parameter", () => {
            const state = mockState();

            const action = AppAction.loadCurrentListOfDegree();
            const result = AppReducer(state, action);

            expect(result.currentDegree.pageNumber).toEqual(1);
        });
    });

    describe("CURRENT_LIST_OF_DEGREE_FAILURE", () => {
        it("should not return value", () => {
            const state = mockState();

            const action = AppAction.currentListOfDegreeFailure(new Error("Test error"));
            const result = AppReducer(state, action);

            expect(result.currentDegree.loadStatus).toEqual(AsyncLoadStatus.FAILURE);
        });
    });

    describe("CURRENT_LIST_OF_DEGREE_SUCCESS", () => {
        it("should return state and set status to success when request is success", () => {
            const state = mockState();
            state.currentDegree.loadStatus = AsyncLoadStatus.SUCCESS;

            const listOfDegree = createCurrentListOfDegree(
                [
                    {
                        degreeCode: "C2000",
                        degreeTitle: "Bachelor of Information Technology",
                        lowestScore: 80,
                        campus: "Clayton"
                    } as DegreeDetailsResponseLine,
                ],
                2,
                1
            );
            const action = AppAction.currentListOfDegreeSuccess(listOfDegree);
            const result = AppReducer(state, action);

            expect(result.currentDegree.loadStatus).toEqual(AsyncLoadStatus.SUCCESS);
            expect(result.currentDegree.totalPages).toEqual(listOfDegree.totalPages);
            expect(result.currentDegree.totalDegrees).toEqual(listOfDegree.totalDegree);
            expect(result.currentDegree.degree).toEqual(listOfDegree.degree);
        });
    });

    describe("LOAD_ITEM_DETAILS", () => {
        it("should not modify the current total degree details ", function () {
            const state = mockState();

            const action = AppAction.loadDegreeDetails({
                degreeCode: "C2000"
            });

            const result = AppReducer(state, action);

            expect(result.degreeDetails).toEqual({
                id: 1,
                degreeCode:"C2000",
                degreeTitle: "Bachelor of Information Technology",
                type: "Bachelor",
                description: "Bachelor for IT students",
                campus: "Clayton",
                lowestScore: 80,
                guaranteedScore: 75,
                lowestAtar: 79
            });
        });

        it("should take degreeCode as a parameter", function () {
            const state = mockState();

            const action = AppAction.loadDegreeDetails({
                degreeCode: "C2000"
            });

            const result = AppReducer(state, action);

            expect(result.degreeDetails.type).toEqual("Bachelor");
            expect(result.degreeDetails.degreeCode).toEqual("C2000");
            expect(result.degreeDetailsLoadStatus).toEqual(AsyncLoadStatus.LOADING);
        });
    });

    describe("DEGREE_DETAILS_FAILURE", () => {
        it("should return current state ", function () {
            const state = mockState();

            const action = AppAction.degreeDetailsFailure(new Error("Test error"));

            const result = AppReducer(state, action);

            expect(result.degreeDetails).toEqual({
                id: 1,
                degreeCode:"C2000",
                degreeTitle: "Bachelor of Information Technology",
                type: "Bachelor",
                description: "Bachelor for IT students",
                campus: "Clayton",
                lowestScore: 80,
                guaranteedScore: 75,
                lowestAtar: 79
            });
            expect(result.degreeDetailsLoadStatus).toEqual(AsyncLoadStatus.FAILURE);
        });
    });

    describe("ITEM_DETAILS_SUCCESS", () => {
        it("should return new state when request is successfully", function () {
            const state = mockState();

            const itemDetail = createNewDegreeDetails({
                id: 1,
                degreeCode:"C2000",
                degreeTitle: "Bachelor of Information Technology",
                type: "Bachelor",
                description: "Bachelor for IT students",
                campus: "Clayton",
                lowestScore: 80,
                guaranteedScore: 75,
                lowestAtar: 79
            }) as DegreeDetailsResponse;

            const action = AppAction.degreeDetailsSuccess(itemDetail);
            state.degreeDetailsLoadStatus = AsyncLoadStatus.SUCCESS;

            const result = AppReducer(state, action);

            expect(result.degreeDetails).toEqual({
                id: 1,
                degreeCode:"C2000",
                degreeTitle: "Bachelor of Information Technology",
                type: "Bachelor",
                description: "Bachelor for IT students",
                campus: "Clayton",
                lowestScore: 80,
                guaranteedScore: 75,
                lowestAtar: 79
            });
            expect(result.degreeDetailsLoadStatus).toEqual(AsyncLoadStatus.SUCCESS);
        });
    });

    const mockState = (): AppState => ({
        ...AppInitialState,
    });

    const createCurrentListOfDegree = (
        degree: DegreeDetailsResponseLine[],
        totalPage: number,
        totalDegree: number
    ): ListOfDegreeResponse => ({
        degree: degree,
        totalPages: totalPage,
        totalDegree: totalDegree,
    });

    const createNewDegreeDetails = (
        degreeDetails: DegreeDetails
    ): DegreeDetailsResponse => ({
        id: degreeDetails.id,
        degreeCode: degreeDetails.degreeCode,
        degreeTitle: degreeDetails.degreeTitle,
        type: degreeDetails.type,
        description: degreeDetails.description,
        campus: degreeDetails.campus,
        lowestScore: degreeDetails.lowestScore,
        guaranteedScore: degreeDetails.guaranteedScore,
        lowestAtar: degreeDetails.lowestAtar

    });
});
