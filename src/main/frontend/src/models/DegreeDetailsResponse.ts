export interface DegreeDetailsResponse {
    id: number;
    degreeCode: string;
    degreeTitle: string;
    type: string;
    description: string;
    campus: string;
    lowestScore: number;
    guaranteedScore: number;
    lowestAtar: number
}