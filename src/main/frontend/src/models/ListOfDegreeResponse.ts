export interface CurrentListOfDegreeResponse extends ListOfDegreeResponse {
    degrees: DegreeDetailsResponseLine[];
}

export interface ListOfDegreeResponse {
    degree: DegreeDetailsResponseLine[];
    totalPages: number;
    totalDegree: number;
}

export interface DegreeDetailsResponseLine {
    degreeCode: string;
    degreeTitle: string;
    lowestScore: number;
    campus: string
}
