export enum AsyncLoadStatus {
    INITIAL,
    LOADING,
    FAILURE,
    SUCCESS,
}
