import { renderHook, act } from "@testing-library/react-hooks";
import useReducerWithEffects, {
    ActionWithEffects,
} from "./useReducerWithEffects";
import { Reducer } from "react";

interface TestState {
    count: number;
}

interface TestEffectContext {
    runEffect: () => Promise<void>;
}

interface TestAction
    extends ActionWithEffects<
        TestState,
        { type: "INCREMENT" },
        TestEffectContext
        > {
    type: "INCREMENT";
}

jest.useFakeTimers();

describe("useReducerWithEffects", () => {
    let initialState: TestState;
    let effectContext: TestEffectContext;
    let testReducer: Reducer<TestState, TestAction>;

    beforeEach(() => {
        initialState = {
            count: 0,
        };

        effectContext = {
            runEffect: jest.fn().mockImplementation(async () => {}),
        };

        testReducer = (prevState, action) => {
            if (action.type === "INCREMENT") {
                return { ...prevState, count: prevState.count + 1 };
            } else {
                return prevState;
            }
        };
    });

    it("should provide initial state and dispatch function at the first call", function () {
        const { result } = renderHook(() =>
            useReducerWithEffects(testReducer, initialState, effectContext)
        );

        expect(result.current).toEqual([initialState, expect.any(Function)]);
    });

    it("should handle actions that don't have effects", function () {
        const { result } = renderHook(() =>
            useReducerWithEffects(testReducer, initialState, effectContext)
        );

        const dispatch = result.current[1];

        act(() => {
            dispatch({ type: "INCREMENT" });
        });

        act(() => {
            jest.advanceTimersToNextTimer();
        });

        expect(result.current[0]).toEqual({ count: 1 });
    });

    it("should handle actions that do have effects", async () => {
        const incrementAsyncSideEffect = () =>
            Promise.resolve<TestAction>({ type: "INCREMENT" });

        const { result, waitForNextUpdate } = renderHook(() =>
            useReducerWithEffects(testReducer, initialState, effectContext)
        );
        const dispatch = result.current[1];

        act(() => {
            dispatch({ type: "INCREMENT", effects: incrementAsyncSideEffect });
        });

        act(() => {
            jest.advanceTimersToNextTimer();
        });

        await Promise.resolve();
        act(() => {
            jest.advanceTimersToNextTimer();
        });

        expect(result.current[0]).toEqual({ count: 2 });
    });
});
