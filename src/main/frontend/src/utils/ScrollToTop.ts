export const ScrollToTop = () =>
  setTimeout(() => {
    if ("scrollBehavior" in document.documentElement.style)
      window.scroll({ top: 0, left: 0, behavior: "smooth" });
    else window.scroll(0, 0);
  }, 100);
