export const joinClasses = (...args: string[]): string => args.join(" ");
