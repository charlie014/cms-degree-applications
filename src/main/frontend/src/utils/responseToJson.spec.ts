import responseToJson from "./responseToJson";

describe("responseToJson", () => {
    // Reject json return when status is invalid
    it("should reject promise if response status is < 200", (done) => {
        responseToJson(new Response(null, { status: 199 }))
            .then(() => fail("The promise should have been rejected"))
            .catch(() => done());
    });

    // Reject json return when status is invalid
    it("should reject or throw an exception if response status is > 299", (done) => {
        responseToJson(new Response(null, { status: 300 }))
            .then(() => fail("The promise should have been rejected"))
            .catch(() => done());
    });

    // Return json return when status is valid
    it("should return to JSON response if the status is between 200 and 299 inclusive", async () => {
        // Stringify is the way we can convert anything to JSON
        const response200 = new Response(JSON.stringify({ test: "TEST1" }), {
            status: 200,
        });

        const response299 = new Response(JSON.stringify({ test: "TEST2" }), {
            status: 299,
        });

        expect(await responseToJson(response200)).toEqual({ test: "TEST1" });
        expect(await responseToJson(response299)).toEqual({ test: "TEST2" });
    });
});
