package cms.degree.applications.consumer;

import au.com.dius.pact.consumer.Pact;
import au.com.dius.pact.consumer.PactProviderRuleMk2;
import au.com.dius.pact.consumer.PactVerification;
import au.com.dius.pact.consumer.dsl.PactDslWithProvider;
import au.com.dius.pact.model.RequestResponsePact;
import cms.degree.applications.service.client.Degree;
import cms.degree.applications.service.client.DegreeServiceClient;
import org.junit.Rule;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.util.HashMap;
import java.util.Map;

public class CMSDegreeServiceDegreeDetailsConsumerTest {

    @Rule
    public PactProviderRuleMk2 mockProvider = new PactProviderRuleMk2("CMSDegreeService", "localhost",
            8080, this);

    @Pact(consumer = "CMSDegreeApplications")
    public RequestResponsePact createPactForRetrievingDegreeDetails(PactDslWithProvider builder) {
        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");

        return builder
                .given("a request retrieving degree details")
                .uponReceiving("A request to retrieve degree details code C2000")
                .path("/api/degree/C2000")
                .method("GET")
                .willRespondWith()
                .status(200)
                .headers(headers)
                .body("{" +
                        " \"id\": \"1\"," +
                        " \"degreeCode\": \"C2000\"," +
                        " \"degreeTitle\": \"Bachelor of Information Technology\"," +
                        " \"type\": \"Bachelor\"," +
                        " \"description\": \"Bachelor for IT students\"," +
                        " \"campus\": \"Clayton\"," +
                        "  \"lowestScore\": \"80\"," +
                        " \"guaranteedScore\": \"75\"," +
                        " \"lowestAtar\": \"79\"" +
                        "}")
                .toPact();
    }

    @Test
    @PactVerification()
    public void retrieveDegreeDetails() throws Exception {
        System.setProperty("pact.rootDir", "../pacts");
        Degree response = new DegreeServiceClient("localhost", mockProvider.getPort())
                .retrieveDegreeByCode("C2000");
        Degree expected = new Degree(
                1,
                "C2000",
                "Bachelor of Information Technology",
                "Bachelor",
                "Bachelor for IT students",
                "Clayton",
                80,
                75,
                79
        );
        Assertions.assertEquals(expected.getCampus(), response.getCampus());
        Assertions.assertEquals(expected.getDegreeCode(), response.getDegreeCode());
    }
}


