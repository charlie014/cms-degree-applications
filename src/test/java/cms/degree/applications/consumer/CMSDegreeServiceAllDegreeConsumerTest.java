package cms.degree.applications.consumer;

import au.com.dius.pact.consumer.Pact;
import au.com.dius.pact.consumer.PactProviderRuleMk2;
import au.com.dius.pact.consumer.PactVerification;
import au.com.dius.pact.consumer.dsl.PactDslWithProvider;
import au.com.dius.pact.model.RequestResponsePact;
import cms.degree.applications.service.client.Degree;
import cms.degree.applications.service.client.DegreeServiceClient;
import org.junit.Rule;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class CMSDegreeServiceAllDegreeConsumerTest {

    @Rule
    public PactProviderRuleMk2 mockProvider = new PactProviderRuleMk2("CMSDegreeService", "localhost",
            8080, this);

    @Pact(consumer = "CMSDegreeApplications")
    public RequestResponsePact createPactForRetrievingAllDegree(PactDslWithProvider builder) {
        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");

        return builder
                .given("a request retrieving all degrees")
                .uponReceiving("A request to retrieve all degrees")
                .path("/api/alldegree")
                .method("GET")
                .willRespondWith()
                .status(200)
                .headers(headers)
                .body("[{" +
                        " \"id\": \"1\"," +
                        " \"degreeCode\": \"C2000\"," +
                        " \"degreeTitle\": \"Bachelor of Information Technology\"," +
                        " \"type\": \"Bachelor\"," +
                        " \"description\": \"Bachelor for IT students\"," +
                        " \"campus\": \"Clayton\"," +
                        "  \"lowestScore\": \"80\"," +
                        " \"guaranteedScore\": \"75\"," +
                        " \"lowestAtar\": \"79\"" +
                        "}," +
                        "{" +
                        " \"id\": \"2\"," +
                        " \"degreeCode\": \"C2001\"," +
                        " \"degreeTitle\": \"Bachelor of Computer Science\"," +
                        " \"type\": \"Bachelor\"," +
                        " \"description\": \"Bachelor for CS students\"," +
                        " \"campus\": \"Clayton\"," +
                        "  \"lowestScore\": \"85\"," +
                        " \"guaranteedScore\": \"79\"," +
                        " \"lowestAtar\": \"80\"" +
                        "}]")
                .toPact();
    }

    @Test
    @PactVerification()
    public void retrieveAllDegree() throws Exception {
        System.setProperty("pact.rootDir", "../pacts");
        ResponseEntity<Degree[]> response = new DegreeServiceClient("localhost", mockProvider.getPort())
                .retrieveAllDegree();
        Degree expected = new Degree(
                1,
                "C2000",
                "Bachelor of Information Technology",
                "Bachelor",
                "Bachelor for IT students",
                "Clayton",
                80,
                75,
                79
        );
        Assertions.assertEquals(Arrays.stream(Objects.requireNonNull(response.getBody())).findFirst().get().getDegreeCode(),
                expected.getDegreeCode());
        Assertions.assertEquals(Arrays.stream(response.getBody()).findFirst().get().getCampus(),
                expected.getCampus());
    }
}
