package cms.degree.applications.service.client;


import cms.degree.applications.exception.DegreeExistException;
import cms.degree.applications.exception.DegreeNotFoundException;
import cms.degree.applications.exception.NoContentDegreeListException;
import com.github.tomakehurst.wiremock.WireMockServer;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

import static com.github.tomakehurst.wiremock.client.WireMock.*;

public class DegreeServiceClientTest {

    @Autowired
    private DegreeServiceClient degreeServiceClient;

    private WireMockServer wireMockServer;

    private Degree expected = new Degree(
            1,
            "C2000",
            "Bachelor of Information Technology",
            "Bachelor",
            "Bachelor for IT students",
            "Clayton",
            80,
            75,
            79
    );

    @BeforeEach
    void setUp() {
        wireMockServer = new WireMockServer();
        configureFor("localhost", 8080);
        wireMockServer.start();

        degreeServiceClient = new DegreeServiceClient("localhost", wireMockServer.port());
    }

    @Test
    public void shouldReturnListOfDegree() throws Exception {
        stubFor(get(urlPathEqualTo("/api/service/alldegree"))
                .willReturn(okJson("" +
                        "[{" +
                        " \"id\": \"1\"," +
                        " \"degreeCode\": \"C2000\"," +
                        " \"degreeTitle\": \"Bachelor of Information Technology\"," +
                        " \"type\": \"Bachelor\"," +
                        " \"description\": \"Bachelor for IT students\"," +
                        " \"campus\": \"Clayton\"," +
                        " \"lowestScore\": \"80\"," +
                        " \"guaranteedScore\": \"75\"," +
                        " \"lowestAtar\": \"79\"" +
                        "}]")));
        ResponseEntity<Degree[]> response = degreeServiceClient.retrieveAllDegree();
        List<Degree> newList = Collections.singletonList(new Degree(
                1,
                "C2000",
                "Bachelor of Information Technology",
                "Bachelor",
                "Bachelor for IT students",
                "Clayton",
                80,
                75,
                79
        ));
        Assertions.assertEquals(Objects.requireNonNull(response.getBody()).length, 1);

        verify(getRequestedFor(urlEqualTo("/api/service/alldegree")));
    }

    @Test
    public void shouldReturnDegreeByCode() throws Exception {
        stubFor(get(urlPathEqualTo("/api/service/degree/C2000"))
                .willReturn(okJson(
                        "{" +
                                " \"id\": \"1\"," +
                                " \"degreeCode\": \"C2000\"," +
                                " \"degreeTitle\": \"Bachelor of Information Technology\"," +
                                " \"type\": \"Bachelor\"," +
                                " \"description\": \"Bachelor for IT students\"," +
                                " \"campus\": \"Clayton\"," +
                                " \"lowestScore\": \"80\"," +
                                " \"guaranteedScore\": \"75\"," +
                                " \"lowestAtar\": \"79\"" +
                                "}"))
                );
        Degree response = degreeServiceClient.retrieveDegreeByCode(expected.getDegreeCode());
        Assertions.assertEquals(expected.getDegreeCode(), response.getDegreeCode());
        Assertions.assertEquals(expected.getCampus(), response.getCampus());
        verify(getRequestedFor(urlEqualTo("/api/service/degree/C2000")));
    }

    @Test
    public void shouldThrow404WhenServiceClientReturnNotFound() {
        Exception thrown = Assertions.assertThrows(DegreeNotFoundException.class, () ->
                degreeServiceClient.retrieveDegreeByCode("C200000")
        );
        Assertions.assertTrue(thrown.getMessage().contains("Unable to find degree code C200000"));
    }

    @Test
    public void shouldThrowEmptyListExceptionWhenServiceClientReturnsEmptyList() {
        stubFor(get(urlPathEqualTo("/api/service/alldegree"))
                .willReturn(
                        aResponse()
                        .withStatus(204)
                )
        );
        Exception thrown = Assertions.assertThrows(NoContentDegreeListException.class, () ->
                degreeServiceClient.retrieveAllDegree()
        );
        Assertions.assertTrue(thrown.getMessage().contains("No Content Degree List"));
    }

    //TODO: fixing the stub for testing
    @Test
    @Disabled
    public void shouldInsertNewDegreeWSuccessfully() throws Exception {
        Degree expectedInsert = new Degree(
                "C2000",
                "Bachelor of Information Technology",
                "Bachelor",
                "Bachelor for IT students",
                "Clayton",
                80,
                75,
                79
        );
        stubFor(post(urlPathEqualTo("/api/service/degree/create"))
                .withHeader("Content-Type", equalTo("application/json"))
                .withRequestBody(equalToJson(
                        "{" +
                                " \"degreeCode\": \"C2000\"," +
                                " \"degreeTitle\": \"Bachelor of Information Technology\"," +
                                " \"type\": \"Bachelor\"," +
                                " \"description\": \"Bachelor for IT students\"," +
                                " \"campus\": \"Clayton\"," +
                                " \"lowestScore\": \"80\"," +
                                " \"guaranteedScore\": \"75\"," +
                                " \"lowestAtar\": \"79\"" +
                                "}",
                        true, true
                ))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "text/plain")
                        .withBody(String.valueOf(okJson(
                        "{" +
                                " \"degreeCode\": \"C2000\"," +
                                " \"degreeTitle\": \"Bachelor of Information Technology\"," +
                                " \"type\": \"Bachelor\"," +
                                " \"description\": \"Bachelor for IT students\"," +
                                " \"campus\": \"Clayton\"," +
                                " \"lowestScore\": \"80\"," +
                                " \"guaranteedScore\": \"75\"," +
                                " \"lowestAtar\": \"79\"" +
                                "}"
                ))))
        );

        Degree response = degreeServiceClient.insertNewDegree(expectedInsert);
        Assertions.assertEquals(expectedInsert.getCampus(), response.getCampus());
        verify(postRequestedFor(urlEqualTo("/api/service/degree/create")));

    }

    @Test
    @Disabled
    public void shouldThrowExceptionWhenDegreeExist() {
        Degree expected = new Degree(
                "C2000",
                "Bachelor of Information Technology",
                "Bachelor",
                "Bachelor for IT students",
                "Clayton",
                80,
                75,
                79
        );
        stubFor(post(urlPathEqualTo("/api/service/degree/create"))
            .willReturn(
                    aResponse()
                    .withStatus(400)
            )
        );
        Exception thrown = Assertions.assertThrows(DegreeExistException.class,
                () -> degreeServiceClient.insertNewDegree(expected)
        );
        Assertions.assertTrue(thrown.getMessage().contains("Degree already exist"));
    }

    @AfterEach
    void tearDown() {
        wireMockServer.stop();
    }
}
