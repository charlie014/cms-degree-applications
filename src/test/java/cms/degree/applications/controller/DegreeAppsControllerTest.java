package cms.degree.applications.controller;

import cms.degree.applications.service.client.Degree;
import cms.degree.applications.usecase.GetAllDegreeUseCase;
import cms.degree.applications.usecase.GetDegreeByCodeUseCase;
import cms.degree.applications.usecase.InsertNewDegreeUseCase;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.testcontainers.shaded.com.fasterxml.jackson.databind.ObjectMapper;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebMvcTest(DegreeAppsController.class)
public class DegreeAppsControllerTest {

    @Autowired
    private MockMvc mockMvc;

    ObjectMapper mapper = new ObjectMapper();

    @MockBean
    private GetAllDegreeUseCase getAllDegreeUseCase;

    @MockBean
    private GetDegreeByCodeUseCase getDegreeByCodeUseCase;

    @MockBean
    private InsertNewDegreeUseCase insertNewDegreeUseCase;

    private DegreeAppsController degreeAppsController;

   @BeforeEach
   void setUp() {
       this.degreeAppsController = new DegreeAppsController(
               getAllDegreeUseCase,
               getDegreeByCodeUseCase,
               insertNewDegreeUseCase
       );
   }

    @Test
    public void shouldRetrieveListOfDegree() throws Exception {
        Degree[] listOfDegree = new Degree[] {
                new Degree(
                        1,
                        "C2000",
                        "Bachelor of Information Technology",
                        "Bachelor",
                        "Bachelor for IT students",
                        "Clayton",
                        80,
                        75,
                        79),
                new Degree(
                        1,
                        "C2001",
                        "Bachelor of Computer Science",
                        "Bachelor",
                        "Bachelor for CS students",
                        "Clayton",
                        85,
                        79,
                        82
                )
        };
        Mockito.when(getAllDegreeUseCase.retrieveAllDegree())
                .thenReturn(ResponseEntity.ok(listOfDegree));
        String url = "/api/apps/degree/getAllDegree";
        MvcResult response = mockMvc.perform(MockMvcRequestBuilders
                .get(url)
                .accept(APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn();
        String actual = response.getResponse().getContentAsString();
        String expected = mapper.writeValueAsString(listOfDegree);
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void shouldReturnDegreeDetails() throws Exception {
       Degree degree = new Degree(
               1,
               "C2001",
               "Bachelor of Computer Science",
               "Bachelor",
               "Bachelor for CS students",
               "Clayton",
               85,
               79,
               82
       );
       Mockito.when(getDegreeByCodeUseCase.retrieveDegreeByCode(degree.getDegreeCode()))
               .thenReturn(degree);
       String url = "/api/apps/degree/details/C2001";
       MvcResult response = mockMvc.perform(MockMvcRequestBuilders
               .get(url)
               .accept(APPLICATION_JSON_VALUE))
               .andExpect(status().isOk())
               .andReturn();
       String actual = response.getResponse().getContentAsString();
       String expected = mapper.writeValueAsString(degree);
       Assertions.assertEquals(expected, actual);
    }

    @Test
    public void shouldInsertNewDegree() throws Exception {
        Degree degree = new Degree(
                "C2001",
                "Bachelor of Computer Science",
                "Bachelor",
                "Bachelor for CS students",
                "Clayton",
                85,
                79,
                82
        );
        Mockito.when(insertNewDegreeUseCase.insertNewDegree(any(Degree.class)))
                .thenReturn(degree);
        String url = "/api/apps/degree/create";
        MvcResult response = mockMvc.perform(MockMvcRequestBuilders
                .post(url)
                .contentType(APPLICATION_JSON_VALUE)
                .content(mapper.writeValueAsString(degree))
                .accept(APPLICATION_JSON_VALUE))
                .andExpect(status().isCreated())
                .andReturn();
        String actual = response.getResponse().getContentAsString();
        String expected = mapper.writeValueAsString(degree);
        Assertions.assertEquals(expected, actual);
    }
}
