package cms.degree.applications.component;

import cms.degree.applications.controller.DegreeAppsController;
import cms.degree.applications.service.client.Degree;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.testcontainers.shaded.com.fasterxml.jackson.databind.ObjectMapper;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class DegreeAppsComponentTestUsingMockMvc {

    @Autowired
    private MockMvc mockMvc;

    ObjectMapper mapper = new ObjectMapper();

    @Autowired
    private DegreeAppsController degreeAppsController;

    @Test
    public void shouldReturnDegreeDetails() throws Exception {
        String url = "/apps/api/degree/details/C2000";

        MvcResult response = mockMvc.perform(MockMvcRequestBuilders
                .get(url)
                .accept(APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn();
        String actual = response.getResponse().getContentAsString();
        Assertions.assertEquals(mapper.writeValueAsString(new Degree(
                1,
                "C2000",
                "Bachelor of Information Technology",
                "Bachelor",
                "Bachelor for IT students",
                "Clayton",
                80,
                79,
                75
        )), actual);
    }

    @Test
    public void shouldReturnListOfDegree() throws Exception {
        String url = "/apps/api/degree/getAllDegree";

        mockMvc.perform(MockMvcRequestBuilders
                .get(url)
                .accept(APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());
    }
}
