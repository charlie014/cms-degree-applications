package cms.degree.applications.component;

import cms.degree.applications.controller.DegreeAppsController;
import cms.degree.applications.service.client.Degree;
import com.github.tomakehurst.wiremock.WireMockServer;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;

import static com.github.tomakehurst.wiremock.client.WireMock.*;

@SpringBootTest
@Disabled
public class DegreeAppsComponentTest {

    @Autowired
    private DegreeAppsController degreeAppsController;

    private WireMockServer wireMockServer;

    @BeforeEach
    void setUp() {
        wireMockServer = new WireMockServer();
        configureFor("localhost", 8092);
        wireMockServer.start();
    }

    @Test
    @Disabled
    public void shouldReturnListOfDegree() throws Exception {
        stubFor(get(urlPathEqualTo("/apps/api/degree/getAllDegree"))
                .willReturn(
                        aResponse()
                        .withStatus(200)
                        .withBody("" +
                                "[{" +
                                " \"id\": \"1\"," +
                                " \"degreeCode\": \"C2000\"," +
                                " \"degreeTitle\": \"Bachelor of Information Technology\"," +
                                " \"type\": \"Bachelor\"," +
                                " \"description\": \"Bachelor for IT students\"," +
                                " \"campus\": \"Clayton\"," +
                                "  \"lowestScore\": \"80\"," +
                                " \"guaranteedScore\": \"79\"," +
                                " \"lowestAtar\": \"75\"" +
                                "}]")));
        ResponseEntity<Degree[]> actual = degreeAppsController.retrieveAllDegrees();
        Degree[] listDegree = new Degree[] {
                new Degree(
                        1,
                        "C2000",
                        "Bachelor of Information Technology",
                        "Bachelor",
                        "Bachelor for IT students",
                        "Clayton",
                        80,
                        79,
                        75
                )
        };
        ResponseEntity<Degree[]> expected = ResponseEntity.ok(listDegree);
        Assertions.assertEquals(expected, actual);
    }

    @Test
    @Disabled
    public void shouldReturnDegreeDetails() throws Exception {
        Degree expected = new Degree(
                1,
                "C2000",
                "Bachelor of Information Technology",
                "Bachelor",
                "Bachelor for IT students",
                "Clayton",
                80,
                79,
                75
        );
        stubFor(get(urlPathEqualTo("/apps/api/degree/details/C2000"))
                .willReturn(
                        aResponse()
                        .withStatus(200)
                                .withBody(String.valueOf(okJson("{" +
                                        " \"id\": \"1\"," +
                                        " \"degreeCode\": \"C2000\"," +
                                        " \"degreeTitle\": \"Bachelor of Information Technology\"," +
                                        " \"type\": \"Bachelor\"," +
                                        " \"description\": \"Bachelor for IT students\"," +
                                        " \"campus\": \"Clayton\"," +
                                        " \"lowestScore\": \"80\"," +
                                        " \"guaranteedScore\": \"79\"," +
                                        " \"lowestAtar\": \"75\"" +
                                        "}")))));

        Degree actual = degreeAppsController.retrieveDegreeByCode(expected.getDegreeCode());
        Assertions.assertEquals(expected, actual);

    }

}
