package cms.degree.applications.usecase;

import cms.degree.applications.service.client.Degree;
import cms.degree.applications.service.client.DegreeServiceClient;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

@ExtendWith(MockitoExtension.class)
//@ExtendWith(SpringExtension.class)
public class GetAllDegreeUseCaseTest {

    @Mock
    private DegreeServiceClient degreeServiceClient;

    @InjectMocks
    private GetAllDegreeUseCase getAllDegreeUseCase;

    @Test
    public void shouldReturnListOfDegrees() throws Exception {
        Degree[] degreeList = new Degree[] {new Degree(
                "C2000",
                "Bachelor of Information Technology",
                "Bachelor",
                "Bachelor for IT students",
                "Clayton",
                80,
                75,
                78
        )};
        ResponseEntity<Degree[]> expected = ResponseEntity.ok(degreeList);
        Mockito.when(degreeServiceClient.retrieveAllDegree())
                .thenReturn(ResponseEntity.ok(degreeList));
        ResponseEntity<Degree[]> actual = getAllDegreeUseCase.retrieveAllDegree();
        Assertions.assertEquals(expected, actual);
    }
}