package cms.degree.applications.usecase;

import cms.degree.applications.service.client.Degree;
import cms.degree.applications.service.client.DegreeServiceClient;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class InsertNewDegreeUseCaseTest {

    @Mock
    private DegreeServiceClient degreeServiceClient;

    @InjectMocks
    private InsertNewDegreeUseCase insertNewDegreeUseCase;

    @Test
    public void shouldInsertNewDegree() throws Exception {
        Degree degree = new Degree(
                "C2000",
                "Bachelor of Information Technology",
                "Bachelor",
                "Bachelor for IT students",
                "Clayton",
                80,
                75,
                78
        );

        Mockito.when(degreeServiceClient.insertNewDegree(degree))
                .thenReturn(degree);
        Degree actual = insertNewDegreeUseCase.insertNewDegree(degree);
        Assertions.assertEquals(degree, actual);
    }
}
