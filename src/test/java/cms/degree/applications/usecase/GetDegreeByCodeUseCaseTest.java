package cms.degree.applications.usecase;

import cms.degree.applications.service.client.Degree;
import cms.degree.applications.service.client.DegreeServiceClient;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class GetDegreeByCodeUseCaseTest {

    @Mock
    private DegreeServiceClient degreeServiceClient;

    @InjectMocks
    private GetDegreeByCodeUseCase getDegreeByCodeUseCase;

    @Test
    public void shouldReturnDegreeDetails() throws Exception {
        Degree expected = new Degree(
            "C2000",
            "Bachelor of Information Technology",
            "Bachelor",
            "Bachelor for IT students",
            "Clayton",
            80,
            75,
            78
        );
        Mockito.when(degreeServiceClient.retrieveDegreeByCode(expected.getDegreeCode()))
                .thenReturn(expected);
        Degree actual = getDegreeByCodeUseCase.retrieveDegreeByCode("C2000");
        Assertions.assertEquals(expected, actual);
    }
}
