## General documentation
```
Project runs on the port 8092. Build the project:
mvn clean install
```

## Run the project
```
mvn spring-boot:run
```

## Dockerization
Build the image
```
docker build -t cms-degree-app:1.0 .
```

Build the image in the cms-degree-service as well (refer to cms-degree-service)

## Build the docker-compose

Check the docker-compose file
```
docker-compose config
```

Build the docker-compose
```
docker-compose up
```

Create DB instance in the docker-compose

Check the DB instance name in docker ps
```
docker-compose ps
```

Create new instance
```
docker exec -it cms-degree-applications_mysql-degree-database_1 bash
mysql -u root -pdegreewebservice

create table degrees (id int not null auto_increment, degree_code text, degree_title text, degree_type text, description text, campus text, lowest_score int, guaranteed_score int, lowest_atar int, primary key (id) );
insert into degrees (id, degree_code, degree_title, degree_type, description, campus, lowest_score, guaranteed_score, lowest_atar) values (1, 'C2000', 'Bachelor of Information Technology', 'Bachelor', 'Bachelor for IT students', 'Clayton', 80, 79, 75);
insert into degrees (id, degree_code, degree_title, degree_type, description, campus, lowest_score, guaranteed_score, lowest_atar) values (2, 'C2001', 'Bachelor of Computer Science', 'Bachelor', 'Bachelor for CS students', 'Clayton', 85, 80, 79);

commit;
```

Turn off the docker-compose after use
```
docker-compose down
```

